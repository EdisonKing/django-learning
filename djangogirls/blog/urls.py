from django.conf.urls import url
from . import views


urlpatterns = [
    url(r'^$', views.post_list, name="post_list"),  # views.post_list该方法绑定到了 r'^$'指定的页面
    url(r'^post/(?P<pk>[0-9]+)/$', views.post_detail, name="post_detail"),
    url(r'^post/new/$', views.post_new, name="post_new"),

]