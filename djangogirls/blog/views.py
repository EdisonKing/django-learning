from django.shortcuts import render, get_object_or_404, redirect
from django.utils import timezone
from .models import Post      # . means current directory or current application
from .forms import PostForm

def post_list(request):
    posts = Post.objects.filter(published_date__lte=timezone.now()).order_by('published_date')
    return render(request, 'blog/post_list.html', {'posts': posts})


def post_detail(request, pk):
    post = get_object_or_404(Post, pk=pk)
    return render(request, 'blog/post_detail.html', {'post': post})


def post_new(request):
    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            # save method creates and saves a database object from the data bound to form.
            post = form.save(commit=False)    # 获取post， 但是form暂时还不想被保存
            post.author = request.user
            post.published_date = timezone.now()
            post.save()
            return redirect("post_detail", pk=post.pk)    # redirect to the view with name post_detail
    else:
        form = PostForm()

    return render(request, 'blog/post_edit.html', {"form": form})